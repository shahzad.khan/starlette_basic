from starlette.templating import Jinja2Templates

templates = Jinja2Templates(directory='templates')

async def homepage(request):
  return templates.TemplateResponse('index.html', {'request': request})

async def listing(request):
  return templates.TemplateResponse('listing.html', {'request': request})

async def confirmation(request):
  return templates.TemplateResponse('confirmation.html', {'request': request})